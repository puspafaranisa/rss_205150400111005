package com.example.rss_205150400111005;

import java.util.List;

public class Item {
    public String title, pubDate, link, guid, author, thumbnail, description, content;
    public Object enclosure;
    public List<String> categories;

    public Item(String title, String pubDate, String link, String guid, String author, String thumbnail, String desc, String content, Object enclosure, List<String> categories){
        this.title = title;
        this.pubDate = pubDate;
        this.link = link;
        this.guid = guid;
        this.author = author;
        this.thumbnail = thumbnail;
        this.description = desc;
        this.content = content;
        this.enclosure = enclosure;
        this.categories = categories;
    }

    public String getTitle() {
        return title;
    }
    public String getPubDate() {
        return pubDate;
    }
    public String getLink() {
        return link;
    }
    public String getAuthor() {
        return author;
    }
    public String getDescription() {
        return description;
    }
}
